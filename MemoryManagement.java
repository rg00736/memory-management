package urn6630695;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MemoryManagement {
	
	// processes in the system
	private ArrayList<Process> processes;
		
	// The Main Memory
	// maps segment base address to segment
	private LinkedHashMap<Integer, Segment> mainMemory;
	
	// total available memory
	public final int TOTAL_SIZE;
	
	// unordered list of empty space in memory
	private ArrayList<Hole> holes;
	
	// CONSTRUCTOR
	public MemoryManagement(ArrayList<Process> processes, int total_size) {
		this.processes = processes;
		this.mainMemory = new LinkedHashMap<Integer, Segment>();
		this.holes = new ArrayList<Hole>();
		this.TOTAL_SIZE = total_size;
		
		// whole of memory is a hole to begin with
		holes.add(new Hole(0, TOTAL_SIZE));
	}
	
	// input examples here, they are then converted to the appropiate format and fed into allocate()
	public void takeInput(int[] input) {
		ArrayList<Integer> segments = new ArrayList<Integer>();
		for (int i = 1; i < input.length; i++) {
			segments.add(input[i]);
		}
		allocate(getProcess(input[0]), segments);
	}
	
	// allocates and deallocates memory to processes; creates, destroys and resizes segments
	public void allocate(Process process, ArrayList<Integer> segments) {
		
		for (int i = 0; i < segments.size() ; i++) {
			// Positive = allocation/expansion, negative = deallocation/reduction
			if (segments.get(i) > 0) {
				// ALLOCATION
				
				// use this to check whether allocation was successful
				boolean success = false;
				
				//first try without compacting, then with
				boolean compacted = false;
				
				while (!compacted && !success) {
					Segment s = null;
					// do we already have a segment here?
					if (process.getSegmentTable().containsKey(i+1)) {
						// if yes use that segment
						// EXPAND SEGMENT
						for (Segment segment : mainMemory.values()) {
							if (segment.getSegNum() == i+1) {
								s= segment;
							}
						}
						// update this segment
						// can only expand this segment if theres enough space

						// check theres enough space after the segment to expand
						// get hole after segment
						for (Hole h : holes) {
							if (h.getBaseAddress() == (process.getSegmentTable().get(s.getSegNum()) + s.getOffset())) {
								// if theres enough space (ie hole is big enough) expansion may continue
								if (segments.get(i) <= h.getOffset()) {
									// store and remove hole
									Hole hole = h;
									holes.remove(h);
									// update segment offset
									s.setOffset(s.getOffset() + segments.get(i));
									// move and shrink hole
									h.setBaseAddress(h.getBaseAddress() + segments.get(i));
									h.setOffset(h.getOffset() - segments.get(i));
									// update the hole list
									holes.add(hole);
									// and we're done
									success = true;
									break;
								}
							}
						}
					} else {
						// if no make a new segment of the input size
						// ADD SEGMENT
						s = new Segment(process.newSegmentNumber(), segments.get(i));
						// then find somewhere to put it...
						// go through holes and get first one big enough
						for (Hole h : holes) {
							// if this hole is big enough
							if (segments.get(i) <= h.getOffset()) {
								// store and remove the hole
								Hole hole = h;
								holes.remove(h);
								// then put the new segment here
								mainMemory.put(hole.getBaseAddress(),  s);
								// update the segment table for this process
								process.addSegment(s.getSegNum(), hole.getBaseAddress());
								// and assign this process to the segment
								s.assignProcess(process);
								// move and shrink the hole
								hole.setBaseAddress(hole.getBaseAddress() + s.getOffset());
								hole.setOffset(hole.getOffset() - s.getOffset());
								// update the hole list
								holes.add(hole);
								// and we're done
								success = true;
								break;
							}
						}
					}
						
					// if we haven't succeeded and haven't compacted, then compact
					// else if we haven't succeeded and have compacted, then we've run out of memory so throw exception
					// otherwise continue
					if (!success && !compacted) {
						int x = i + 1;
						System.out.println("Process " + process.getPID() + " segment " + x + " failed to allocate " + segments.get(i) + " bytes. Compacting...");
						compact();
						compacted = true;
					} else if (!success && compacted) {	
						// now we've compacted and there's still no space so throw exception
						throw new IllegalArgumentException("No free space left in memory to allocate " + segments.get(i) + " bytes to segment " + i + " process " + process.getPID());
					}
				}
				
				
					
			} else if (segments.get(i) < 0) {
				
				// get the segment we want - loop through segments in main memory
				for (int base : mainMemory.keySet()) {
					Segment s = mainMemory.get(base);
					// if the process and segment number are correct thats the one we want
					if (s.getProcess().equals(process) && s.getSegNum()-1 == i) {
						// if size would be reduced to 0 or below -> DELETE SEGMENT
						if (s.getOffset() + segments.get(i) <= 0) {
							// resize to 0 (as calculated above)
							s.setOffset(0);
							// remove from memory
							mainMemory.remove(base,  s);
							// remove from segment table
							process.removeSegment(s.getSegNum(), base);
							// update holes:
							Hole hole = new Hole(base + s.getOffset(), segments.get(i) * -1);
							holes.add(hole);
							break;
						} else {
							// if it won't be reduced to 0 -> REDUCE SEGMENT
							s.setOffset(s.getOffset() + segments.get(i));
							// make and add new hole to fill free space
							// address is upper address of reduced segment, offset is amount that that segment was reduced by
							Hole hole = new Hole(base + s.getOffset(), segments.get(i) * -1);
							holes.add(hole);
							break;
						}
						
						
						
					}
				}			
			}
		}
	}
	

	// moves all segments together to start of memory, so all remaining empty space is consecutive
	public void compact() {
		
		// put all segments in a list
		ArrayList<Segment> memSegments = new ArrayList<Segment>();
		
		for (Segment s : mainMemory.values()) {
			memSegments.add(s);
		}
		
		// clear memory
		mainMemory.clear();
		// put segments back
		int address = 0;
		for (Segment s : memSegments) {
			// put it back in memory
			mainMemory.put(address, s);
			// update relevant segment table
			s.getProcess().updateSegment(s.getSegNum(), address);
			
			address = address + s.getOffset();
		}
		
		// clear holes
		holes.clear();
		// put one large hole back
		Hole hole = new Hole(address, TOTAL_SIZE - address);
		holes.add(hole);
		
	}
	
	// gets a process from processes array given a process ID
	// used in takeExample()
	private Process getProcess(int id) {
		for (Process p : processes) {
			if (p.getPID() == id) {
				return p;
			}
		}
		return null;
	}
	
	// returns string representation of processes and their segment tables 
	public String printProcesses() {
		String output = "\nProcesses:";
		for (Process p : processes) {
			output += p.toString();
		}
		return output;
	}
	
	// returns string representation of the contents of main memory
	public String printMainMemory() {
		String output = "\nMain memory:\nTotal size: " + TOTAL_SIZE + " bytes\n\nSegments:\nSegment\t\tBase address";
		for (Integer i : mainMemory.keySet()) {
			// if its a segment, print it
			if (mainMemory.get(i).getClass().getName().equals("urn6630695.Segment")) {
				output += "\n" + ((Segment) mainMemory.get(i)).getSegNum() + " (" + mainMemory.get(i).getOffset() + " bytes)\t\t" + i;
			}
			
		}
		output += "\n" + printHoles();
		return output;
	}
	
	// returns string representation of the list of holes
	public String printHoles() {
		String output = "\nHoles:\nBase address\tOffset";
		for (Hole h : holes) {
			output += "\n" + h.getBaseAddress() + "\t\t" + h.getOffset();
		}
		return output;
	}
	
}
