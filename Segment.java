package urn6630695;

public class Segment {
	// represents a chunk of memory with variable size that is assigned to a process
	
	// Segment number
	private int segNum;
	
	// offset/size
	private int offset;
	
	// process this segment is allocated to
	private Process process;
		
	// CONSTRUCTOR
	public Segment(int segNum, int offset) {
		this.offset = offset;
		this.segNum = segNum;
	}
	
	// GETTERS
	public int getSegNum() {
		return this.segNum;
	}

	public Process getProcess() {
		return this.process;
	}
	
	public int getOffset() {
		return offset;
	}
	
	// SETTERS

	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public void assignProcess(Process p) {
		this.process = p;
	}

	
}
