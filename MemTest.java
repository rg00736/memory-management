package urn6630695;

import java.util.ArrayList;

public class MemTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Print code location for A.1.2:
		System.out.println();
		System.out.println("Start Code Location A.1.2");
		System.out.println("File urn6630695/MemoryManagement.java, line 45-175");
		System.out.println("End Code Location A.1.2");
		System.out.println();
		
		// Example A.1.2. - Allocation and deallocation of processes from memory
		System.out.println("Start example A.1.2");
		Process p1= new Process(1);
		Process p2= new Process(2);
		Process p3= new Process(3);
		Process p4= new Process(4);
		Process p5= new Process(5);
		ArrayList<Process> processes = new ArrayList<Process>();
		processes.add(p1);
		processes.add(p2);
		processes.add(p3);
		processes.add(p4);
		processes.add(p5);
		MemoryManagement manager = new MemoryManagement(processes, 1024);
		System.out.println("\nBefore any allocation: ");
		System.out.println(manager.printMainMemory());
		System.out.println(manager.printProcesses());
		
		System.out.println("\nAllocating 'int[] example1 = {1, 100, 200, 10}'");
		int[] example1 = {1, 100, 200, 10};
		manager.takeInput(example1);
		System.out.println(manager.printMainMemory());
		System.out.println(manager.printProcesses());
		
		System.out.println("\nAllocating 'int[] example2 = {1, -40, -200, 10}'");
		int[] example2 = {1, -40, -200, 10};
		manager.takeInput(example2);
		System.out.println(manager.printMainMemory());
		System.out.println(manager.printProcesses());
		
		System.out.println("\nAllocating 'int[] example3 = {1, -40, 200}'");
		int[] example3 = {1, -40, 200};
		manager.takeInput(example3);
		System.out.println(manager.printMainMemory());
		System.out.println(manager.printProcesses());
		
		System.out.println("\nAllocating 'int[] example4 = {2, 100, 50, 10}'");
		int[] example4 = {2, 100, 50, 10};
		manager.takeInput(example4);
		System.out.println(manager.printMainMemory());
		System.out.println(manager.printProcesses());
		
		System.out.println("\nAllocating 'int[] example5 = {3, 100}'");
		int[] example5 = {3, 100};
		manager.takeInput(example5);
		System.out.println(manager.printMainMemory());
		System.out.println(manager.printProcesses());
		
		System.out.println("\nAllocating 'int[] example6 = {3, -100}'");
		int[] example6 = {3, -100};
		manager.takeInput(example6);
		System.out.println(manager.printMainMemory());
		System.out.println(manager.printProcesses());
		
		System.out.println("\nAllocating 'int[] example7 = {4, 70, 300};'");
		int[] example7 = {4, 70, 300};
		manager.takeInput(example7);
		System.out.println(manager.printMainMemory());
		System.out.println(manager.printProcesses());
		
		System.out.println("\nAllocating 'int[] example8 = {10, 10, 10};'");
		int[] example8 = {5, 10, 10, 10, 10, 10, 10};
		manager.takeInput(example8);
		System.out.println(manager.printMainMemory());
		System.out.println(manager.printProcesses());
		
		System.out.println("End example A.1.2");
		 
		// Print code location for A.3.2 Compaction:
		System.out.println();
		System.out.println("Start Code Location A.3.2 Compaction");
		System.out.println("File urn6630695/MemoryManagement.java, line 130 (method call) and lines 179-207 (method implementation)");
		System.out.println("End Code Location A.3.2 Compaction");
		System.out.println();
		
		// Example A.3.2 Compaction:
		System.out.println("Start Example A.3.2 Compaction");
		System.out.println();
		System.out.println("Before compaction:");
		System.out.println();
		System.out.println(manager.printMainMemory());
		System.out.println(manager.printProcesses());
		manager.compact();
		System.out.println();
		System.out.println("After compaction:");
		System.out.println();
		System.out.println(manager.printMainMemory());
		System.out.println(manager.printProcesses());
		System.out.println();
		System.out.println("End Example A.3.2 Compaction");
		
	}

}
