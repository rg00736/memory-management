package urn6630695;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Process {
	// represents a process
	
	// process id number
	private int id;
	
	// The Segment Table
	// maps segment number to base address in main memory
	private LinkedHashMap<Integer, Integer> segmentTable;
	
	// used to assign segment numbers
	private int index;
	
	
	// CONSTRUCTOR
	public Process(int id) {
		this.id = id;
		this.segmentTable = new LinkedHashMap<Integer, Integer>();
		this.index = 0;
	}
	
	// GETTERS
	public int getPID(){
		 return this.id;
	}
	
	public LinkedHashMap<Integer, Integer> getSegmentTable() {
		return segmentTable;
	}
	
	// assign a new segment to this process
	public void addSegment(int num, int baseAddress) {
		segmentTable.put(num, baseAddress);
		index++;
	}
	
	// change a segment's base address
	public void updateSegment(int num, int baseAddress) {
		segmentTable.put(num, baseAddress);
	}
	
	// removes an entry from this process' segment table
	public void removeSegment(int num, int baseAddress) {
		segmentTable.remove(num, baseAddress);
	}
	
	// returns string representation of this process' segment table
	private String printSegmentTable() {
		String output = "\nSegment table:\nNumber\t\tBase address";
		for (Integer i : segmentTable.keySet()) {
			output += "\n" + i + "\t\t" + segmentTable.get(i);
		}
		return output;
	}
	
	// returns string representation of this process
	public String toString() {
		String output = "\n\nProcess " + id;
		output += printSegmentTable();
		return output;
	}
	
	// generates a new usable segment number
	public int newSegmentNumber() {
		return index + 1;
	}
}
