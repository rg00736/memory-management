package urn6630695;

public class Hole {
	// represents empty space in memory
	
	// size
	private int offset;
	
	// start of hole
	private int baseAddress;
	
	// CONSTRUCTOR
	public Hole (int base, int offset) {
		this.baseAddress = base;
		this.offset = offset;
	}
	
	// GETTERS
	public int getOffset() {
		return offset;
	}

	public int getBaseAddress() {
		return baseAddress;
	}
	
	// SETTERS
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public void setBaseAddress(int baseAddress) {
		this.baseAddress = baseAddress;
	}

	


}
